

## Représentation des données : Types et valeurs de base

Toute machine informatique manipule une représentation des données dont l’unité minimale est le bit 0/1, ce qui permet d’unifier logique et calcul. Les données de base sont représentées selon un codage dépendant de leur nature : entiers, flottants, caractères et chaînes de caractères. Le codage conditionne la taille des différentes valeurs en mémoire.

1. [Les nombres entiers](../01_nombres_entiers/cours/)
2. [Les nombres décimaux](../02_nombres_entiers/cours/)
3. [Les nombres entiers](../03_operateurs_booleens/cours/)


## Représentation des données : Types construits

À partir des types de base se constituent des types construits, qui sont introduits au fur et à mesure qu’ils sont nécessaires. Il s’agit de présenter tour à tour les p-uplets (tuples), les enregistrements qui collectent des valeurs de types différents dans des champs nommés et les tableaux qui permettent un accès calculé direct aux éléments. En pratique, on utilise les appellations de Python, qui peuvent être différentes de celles d’autres langages de programmation.

