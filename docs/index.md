## Cahier de textes


!!! note "Séance du Lundi 18/10/2021 et du Mardi 19/10/2021"
    - Activité - Cours : [Les fonctions en Python](05_langages_et_programmation/01_bases_de_python/01_init_python/5.%20Les%20Fonctions.md){:target="_blank"}
    - Activité [France IOI](http://www.france-ioi.org/){:target="_blank"}

### Séances précédentes

??? note "Archives"

    ??? note "Séance du Lundi 04/10/2021 et du Mardi 05/10/2021"
        - Activité - Cours : [Les boucles non bornées](05_langages_et_programmation/01_bases_de_python/01_init_python/4.1%20Les%20boucles%20non%20bornées.md){:target="_blank"}
        - TP Exercices : [Les boucles non bornées](05_langages_et_programmation/01_bases_de_python/01_init_python/4.2%20Exercices%20-%20while.md){:target="_blank"}
        - Activité [France IOI](http://www.france-ioi.org/){:target="_blank"}


    ??? note "Séance du Lundi 27/09/2021 et du Mardi 28/09/2021"
        - Activité - Cours : [Les boucles bornées](05_langages_et_programmation/01_bases_de_python/01_init_python/3.1%20Les%20boucles%20bornées.md){:target="_blank"}
        - TP Exercices : [Les boucles bornées](05_langages_et_programmation/01_bases_de_python/01_init_python/3.2%20Exercices%20-%20for.md){:target="_blank"}
        - Activité [France IOI](http://www.france-ioi.org/){:target="_blank"}

    ??? note "Séance du Lundi 20/09/2021 et du Mardi 21/09/2021"
        - Activité - Cours : [Les instructions conditionnelles](05_langages_et_programmation/01_bases_de_python/01_init_python/2.%20Instruction%20conditionnelle.md){:target="_blank"}
        - Exercices 
        - Activité [France IOI](http://www.france-ioi.org/){:target="_blank"}



    ??? note "Séance du Lundi 13/09/2021 et du Mardi 14/09/2021"
        - Notion de variable en Python.
        - Activité - Cours : [Introduction au langage de programmation Python](05_langages_et_programmation/01_bases_de_python/01_init_python/1.%20Introduction%20Python.md){:target="_blank"}
        - Activité sur les boucles [France IOI](http://www.france-ioi.org/){:target="_blank"}

    ??? note "Séance du Lundi 06/09/2021 et du Mardi 07/09/2021"
        - Vérification adresse @lycee-lorgues.fr.
        - Création du compte pour France IOI
        - Activité variables [France IOI](http://www.france-ioi.org/){:target="_blank"}



## Programme


